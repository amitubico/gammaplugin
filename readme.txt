=== Gamma Plugin ===
Contributors: guptaamit728
Donate link: http://ubicoapps.com/gamma.zip
Tags: comments, spam
Requires at least: 4.0
Tested up to: 5.2.4 
Requires PHP: 5.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.
 
== Description ==
 
This is the long description.  No limit, and you can use Markdown (as well as in the following sections).
 
For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.


Major features in Akismet include:

* Automatically checks all comments and filters out the ones that look like spam.
* Each comment has a status history, so you can easily see which comments were caught or cleared by Akismet and which were spammed or unspammed by a moderator.
* URLs are shown in the comment body to reveal hidden or misleading links.
* Moderators can see the number of approved comments for each user.
* A discard feature that outright blocks the worst spam, saving you disk space and speeding up your site.

== Installation ==
 
Upload the Akismet plugin to your blog, Activate it, then enter your [Akismet.com API key](https://akismet.com/get/).

1, 2, 3: You're done!
 
== Changelog ==
 
= 2.1.0 =
*Release Date - 10 Oct 2019*

* Fixed a conflict between the Akismet setup banner and other plugin notices.
* Reduced the number of API requests made by the plugin when attempting to verify the API key.
* Include additional data in the pingback pre-check API request to help make the stats more accurate.
* Fixed a bug that was enabling the "Check for Spam" button when no comments were eligible to be checked.
* Improved Akismet's AMP compatibility.

= 1.0.0 =
*Release Date - 31 January 2019*

* Fixed the "Setup Akismet" notice so it resizes responsively.
* Only highlight the "Save Changes" button in the Akismet config when changes have been made.
* The count of comments in your spam queue shown on the dashboard show now always be up-to-date.

 