<!----- Stylesheet File ------->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!----- Js File ------->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script>
    $(function(){
        $("#formpost").validate({
            submitHandler:function(){
                console.log($("#formpost").serialize());
            }
        })
    });
</script>

<h1 class="main-heading">General Settings</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <tr>
            <th>Shortcode</th>
            <td>[gamma-plugin]</td>
        </tr>
        <tr>
            <th>Shortcode</th>
            <td>ghjghj</td>
        </tr>
        <tr>
            <th>Author</th>
            <td>ghjghj</td>
        </tr>
        <tr>
            <th>Date</th>
            <td>ghjgh</td>
        </tr>
    </table>
</div>
<h3 class="main-heading">Home Page Settings</h3>
 <form action="#" id="formpost">
    <div class="form-group">
      <label for="title">Home Title:</label>
      <input type="text" class="form-control" id="title" titlemaxlength="57" placeholder="Home Title" name="title" required>
      <span id="rchars1">57</span> characters. Most search engines use a maximum of 57 chars for the home title.
    </div>
    <div class="form-group">
      <label for="description">Home Description:</label>
      <textarea class="form-control" id="description" maxlength="160" placeholder="Home Description" rows="4" name="description" required></textarea> 
      <span id="rchars">160</span> characters. Most search engines use a maximum of 160 chars for the home description.
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  <script>
      var maxLength = 160;
        $('#description').keyup(function() {
          var textlen = maxLength - $(this).val().length;
          $('#rchars').text(textlen);
        });
  </script>
   <script>
      var titlemaxLength = 57;
        $('#title').keyup(function() {
          var textlen = titlemaxLength - $(this).val().length;
          $('#rchars1').text(textlen);
        });
      
  </script>