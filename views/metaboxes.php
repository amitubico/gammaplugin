<?php
add_action('admin_init', function(){
	add_meta_box( '_mycutommetabox', 
				  'My Cutom MetaBox', 
				  'gamma_cutom_metabox', 
				  ['post', 'page']
				);
});

function gamma_cutom_metabox($post){
	$_mymetabox = get_post_meta($post->ID, '_mymetabox', true) ? get_post_meta($post->ID, '_mymetabox', true) : '';
?>
	<input type="text" id="" name="_mymetabox" class="" value="<?php echo $_mymetabox; ?>" />
	<?php 
}
add_action('save_post', 'gamma_save_post');

function gamma_save_post($post_id){
	if(array_key_exists('_mymetabox', $_POST)){
		//print_r($_POST); die();
		update_post_meta($post_id, '_mymetabox', $POST['_mymetabox']);
	}
}
