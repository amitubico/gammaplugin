<!----- Stylesheet File ------->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!----- Js File ------->
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<?php 
	$front_displays = get_option( 'show_on_front' );
	$page_on_front  = get_option( 'page_on_front' );

?>
<h1 class="main-heading">System Status</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
    	<tr>
            <th>Operating System</th>
            <td><?php echo PHP_OS; ?></td>
        </tr>
        <tr>
            <th>Server</th>
            <td><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
        </tr>
        <tr>
            <th>Memory usage</th>
            <td><?php echo memory_get_usage(); ?></td>
        </tr>
        <tr>
            <th>MYSQL Version</th>
            <td><?php echo mysql_get_server_info(); ?></td>
        </tr>
        <tr>
            <th>PHP Version</th>
            <td><?php echo phpversion(); ?></td>
        </tr>
        <tr>
            <th>PHP Allow URL fopen</th>
            <td><?php echo ini_get('allow_url_fopen'); ?></td>
        </tr>
        <tr>
            <th>PHP Memory Limit</th>
            <td><?php echo memory_get_usage() ; ?></td>
        </tr>
        <tr>
            <th>Site URL</th>
            <td><?php echo get_option( 'siteurl' ); ?></td>
        </tr>
        <tr>
            <th>Home URL</th>
            <td><?php echo get_option( 'home' ); ?></td>
        </tr>
        <tr>
            <th>WordPress Version</th>
            <td><?php echo get_bloginfo('version'); ?></td>
        </tr>
        <tr>
            <th>WordPress DB Version</th>
            <td><?php echo get_option( 'db_version' ); ?></td>
        </tr>
        <tr>
            <th>Active Theme</th>
            <td><?php echo wp_get_theme('name'); ?></td>
        </tr>
        <tr>
            <th>Site Title</th>
            <td><?php echo get_bloginfo( 'name' ); ?></td>
        </tr>
        <tr>
            <th>Site Language</th>
            <td><?php echo get_bloginfo( 'language' ); ?></td>
        </tr>
        <tr>
            <th>Search Engine Visibility</th>
            <td><?php echo get_option( 'blog_public' ); ?></td>
        </tr>
        <tr>
            <th>Permalink Setting</th>
            <td><?php echo get_option( 'permalink_structure' ); ?></td>
        </tr>
        <tr>
            <th>Host Server </th>
            <td><?php echo $_SERVER['SERVER_NAME']; ?></td>
        </tr>
        <tr>
        	<th>Browser Details</th>
        	<td><?php echo $_SERVER['HTTP_USER_AGENT']; ?></td>
        </tr>
    </table>
</div>
